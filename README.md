# R

## Content

```
./R. B. Karanov:
R. B. Karanov - Teroare in stepa 1.0 '{ActiuneRazboi}.docx

./R. C. Walker:
R. C. Walker - Act de lupta 0.9 '{SF}.docx

./R. J. Palacio:
R. J. Palacio - Cartea despre Pluto 1.0 '{Tineret}.docx
R. J. Palacio - Cartea lui Julian 1.0 '{Tineret}.docx
R. J. Palacio - Minunea 1.0 '{Tineret}.docx

./R. L. Stine:
R. L. Stine - Atacul mutantilor 1.0 '{Horror}.docx
R. L. Stine - Blestemul preotesei 1.0 '{Horror}.docx
R. L. Stine - Goosebumps - V1 Bun venit in casa mortilor 0.9 '{Horror}.docx
R. L. Stine - Goosebumps - V2 Nu coborati in pivnita 1.0 '{Horror}.docx
R. L. Stine - Goosebumps - V3 Sangele monstrului 1.0 '{Horror}.docx
R. L. Stine - Goosebumps - V4 Zambeste si mori 0.9 '{Horror}.docx
R. L. Stine - Goosebumps - V6 Sa devenim invizibili 0.9 '{Horror}.docx
R. L. Stine - Goosebumps - V7 Noaptea papusii 1.0 '{Horror}.docx
R. L. Stine - Goosebumps - V8 Fetita care a strigat monstrul! 1.0 '{Horror}.docx
R. L. Stine - Goosebumps - V9 Bun venit in tabara de cosmar 1.0 '{Horror}.docx
R. L. Stine - Goosebumps - V14 Varcolacul din mlastina Fever 1.0 '{Horror}.docx
R. L. Stine - Goosebumps - V15 Nu ma puteti speria 1.0 '{Horror}.docx
R. L. Stine - Goosebumps - V16 O zi in tinutul ororilor 1.0 '{Horror}.docx
R. L. Stine - Goosebumps - V19 Necazuri mari 1.0 '{Horror}.docx
R. L. Stine - Goosebumps - V22 Plaja bantuita 0.9 '{Horror}.docx
R. L. Stine - Goosebumps - V24 Fantoma de la teatru 1.0 '{Horror}.docx
R. L. Stine - Goosebumps - V34 Razbunarea piticilor de gradina 1.0 '{Horror}.docx
R. L. Stine - Goosebumps - V38 Teribilul om de zapada din Pasadena 1.0 '{Horror}.docx

./R. M. Alberes:
R. M. Alberes - Istoria romanului modern 0.99 '{Istorie}.docx

./R. M. Kirsch Meyer:
R. M. Kirsch Meyer - Uimitoarele aventuri ale lui Buffalo Bill in vestul salbatic 1.0 '{Western}.docx

./R. Scott Bakker:
R. Scott Bakker - La inceput a fost intunericul 1.0 '{SF}.docx
R. Scott Bakker - Profetul razboinic 1.0 '{SF}.docx

./R. Voss:
R. Voss - Halucinanta caravana a aurului 1.0 '{Tineret}.docx

./Rabindranath Tagore:
Rabindranath Tagore - Sadhana 0.9 '{Literatura}.docx

./Rachel Caine:
Rachel Caine - Vampirii din Morganville - V1 Casa de sticla 1.0 '{Vampiri}.docx
Rachel Caine - Vampirii din Morganville - V2 Balul fetelor moarte 1.0 '{Vampiri}.docx
Rachel Caine - Vampirii din Morganville - V3 Aleea intunericului 1.0 '{Vampiri}.docx
Rachel Caine - Vampirii din Morganville - V4 Banchetul nebunilor 1.0 '{Vampiri}.docx
Rachel Caine - Vampirii din Morganville - V5 Domnia haosului 1.0 '{Vampiri}.docx
Rachel Caine - Vampirii din Morganville - V6 Carpe Corpus 1.0 '{Vampiri}.docx
Rachel Caine - Vampirii din Morganville - V7 Stingerea 1.0 '{Vampiri}.docx
Rachel Caine - Vampirii din Morganville - V8 Sarutul mortii 1.0 '{Vampiri}.docx

./Rachel Cohn:
Rachel Cohn - Playlist pentru Nick si Norah 0.99 '{Literatura}.docx

./Rachel Cusk:
Rachel Cusk - Arlington Park 0.8 '{Diverse}.docx

./Rachel Dixon:
Rachel Dixon - O noua viata 0.9 '{Dragoste}.docx

./Rachel Finding:
Rachel Finding - Concediu prelungit 0.99 '{Romance}.docx
Rachel Finding - Femeia emancipata 0.99 '{Romance}.docx

./Rachel Hawkins:
Rachel Hawkins - Hex Hall 0.9 '{Supranatural}.docx

./Rachel Heston:
Rachel Heston - Cine ma asteapta acasa 0.99 '{Romance}.docx

./Rachel Lee:
Rachel Lee - Admiratorul secret 1.0 '{Romance}.docx
Rachel Lee - Aleasa regizorului 0.99 '{Dragoste}.docx

./Rachel Ward:
Rachel Ward - Numere 1.0 '{AventuraTineret}.docx

./Radu Aldulescu:
Radu Aldulescu - Amantul colivaresei 1.0 '{Diverse}.docx

./Radu Beligan:
Radu Beligan - Un rol nou 1.0 '{Diverse}.docx

./Radu Cinamar:
Radu Cinamar - V1 Viitor cu cap de mort 1.0 '{Spiritualitate}.docx
Radu Cinamar - V2 Misterul din Egipt - Primul tunel 1.0 '{Spiritualitate}.docx
Radu Cinamar - V3 12 zile - O initiere secreta 1.0 '{Spiritualitate}.docx
Radu Cinamar - V4 Pergamentul secret - Cinci tehnici initiatice tibetane 1.0 '{Spiritualitate}.docx
Radu Cinamar - V5 In interiorul pamantului. Al doilea tunel 1.0 '{Spiritualitate}.docx
Radu Cinamar - V6 Geneza uitata 1.0 '{Spiritualitate}.docx
Radu Cinamar - V7 Cristalul eteric 1.0 '{Spiritualitate}.docx

./Radu Ciobanu:
Radu Ciobanu - Dialog peste Atlantic 0.9 '{Convorbiri}.docx
Radu Ciobanu - Nemuritorul albastru 1.0 '{IstoricaRo}.docx
Radu Ciobanu - Ultima vacanta 0.99 '{Literatura}.docx
Radu Ciobanu - Vamile noptii 1.0 '{IstoricaRo}.docx

./Radu Cosasu:
Radu Cosasu - Mi se pare romantic 1.0 '{Teatru}.docx
Radu Cosasu - Un nasture sau absolutul 1.0 '{Teatru}.docx

./Radu Gyr:
Radu Gyr - Balade 0.99 '{Versuri}.docx
Radu Gyr - Lirica orala 0.99 '{Versuri}.docx
Radu Gyr - Poezia in catuse 0.7 '{Versuri}.docx
Radu Gyr - Sangele temnitei 0.99 '{Versuri}.docx
Radu Gyr - Stele pentru leagan 0.9 '{Versuri}.docx
Radu Gyr - Studentimea si idealul spiritual 0.9 '{Tineret}.docx
Radu Gyr - Tara 0.99 '{Versuri}.docx

./Radu Herjeu:
Radu Herjeu - Ramasite de tinerete 1.0 '{Versuri}.docx

./Radu Honga:
Radu Honga - Aventurile lui Theodore 2.0 '{SF}.docx

./Radu Ilie Manecuta:
Radu Ilie Manecuta - Nutritia omului - act sacru 0.9 '{Sanatate}.docx

./Radu Mihai Crisan:
Radu Mihai Crisan - Dincolo de Andromeda 0.9 '{Teatru}.docx

./Radu Nitu:
Radu Nitu - Padurea nu doarme 1.0 '{IstoricaRo}.docx

./Radu Nor:
Radu Nor - Braul albastru 1.0 '{SF}.docx
Radu Nor - Capitolul XXIII 1.0 '{SF}.docx
Radu Nor - Idolul de sticla V1 1.0 '{ClubulTemerarilor}.docx
Radu Nor - Idolul de sticla V2 1.0 '{ClubulTemerarilor}.docx
Radu Nor - Mister in zece ipostaze 1.0 '{SF}.docx
Radu Nor - Reintoarcerea paianjenului 0.4 '{Politista}.docx

./Radu Nor & I. M. Stefan:
Radu Nor & I. M. Stefan - Robinsoni pe planeta oceanelor 0.99 '{SF}.docx
Radu Nor & I. M. Stefan - Taina printului Semempsis 0.9 '{SF}.docx

./Radu Paraschivescu:
Radu Paraschivescu - Cautatorii de povesti 1.0 '{Umor}.docx
Radu Paraschivescu - Ghidul nesimtitului 0.99 '{Umor}.docx
Radu Paraschivescu - Mi-e rau la cap ma doare mintea 0.8 '{Umor}.docx

./Radu Pavel Gheo:
Radu Pavel Gheo - Anomalia 0.9 '{SF}.docx
Radu Pavel Gheo - Dar de Craciun 0.99 '{SF}.docx
Radu Pavel Gheo - Dexul si sexul 0.9 '{SF}.docx
Radu Pavel Gheo - Hey, you, author 0.99 '{SF}.docx
Radu Pavel Gheo - Unchiul nostru Rafael 0.99 '{SF}.docx

./Radu Petrescu:
Radu Petrescu - Matei Iliescu 1.0 '{Dragoste}.docx

./Radu Preda:
Radu Preda - Jurnal cu Petre Tutea 0.9 '{Jurnal}.docx

./Radu Rosetti:
Radu Rosetti - Cu palosul 1.0 '{IstoricaRo}.docx

./Radu Selejan:
Radu Selejan - Roata fara sfarsit 0.6 '{IstoricaRo}.docx

./Radu Sergiu Ruba:
Radu Sergiu Ruba - Constelatia Homer 0.9 '{Versuri}.docx
Radu Sergiu Ruba - Demonul confesiunii 0.9 '{Literatura}.docx
Radu Sergiu Ruba - Dialoguri si eseuri 0.9 '{Spiritualitate}.docx
Radu Sergiu Ruba - Iluzia continua 0.9 '{Versuri}.docx
Radu Sergiu Ruba - Pe colinele Manciuriei 0.99 '{Diverse}.docx

./Radu Theodoru:
Radu Theodoru - Aproape de zei 1.0 '{ActiuneRazboi}.docx
Radu Theodoru - Brazda si palos V1 1.0 '{IstoricaRo}.docx
Radu Theodoru - Brazda si palos V2 1.0 '{IstoricaRo}.docx
Radu Theodoru - Calaretul rosu 1.0 '{IstoricaRo}.docx
Radu Theodoru - Corsarul 1.0 '{IstoricaRo}.docx
Radu Theodoru - Popas in Madagascar V1 1.0 '{ClubulTemerarilor}.docx
Radu Theodoru - Popas in Madagascar V2 1.0 '{ClubulTemerarilor}.docx
Radu Theodoru - Regina de abanos 1.0 '{IstoricaRo}.docx
Radu Theodoru - Stramosii 1.0 '{IstoricaRo}.docx
Radu Theodoru - Taina recifului 1.0 '{IstoricaRo}.docx
Radu Theodoru - Tara fagaduintei 1.0 '{IstoricaRo}.docx
Radu Theodoru - Vitejii V1 1.0 '{IstoricaRo}.docx
Radu Theodoru - Vitejii V2 1.0 '{IstoricaRo}.docx
Radu Theodoru - Vulturul V1 1.0 '{IstoricaRo}.docx
Radu Theodoru - Vulturul V2 1.0 '{IstoricaRo}.docx
Radu Theodoru - Vulturul V3 1.0 '{IstoricaRo}.docx
Radu Theodoru - Vulturul V4 1.0 '{IstoricaRo}.docx

./Radu Tuculescu:
Radu Tuculescu - Uscatoria de partid 0.9 '{Teatru}.docx

./Radu Tudoran:
Radu Tudoran - Acea fata frumoasa 0.8 '{AventuraTineret}.docx
Radu Tudoran - Al optzeci si doilea 0.8 '{AventuraTineret}.docx
Radu Tudoran - Anotimpuri 1.0 '{AventuraTineret}.docx
Radu Tudoran - Fiul risipitor 2.0 '{AventuraTineret}.docx
Radu Tudoran - Flacarile 1.9 '{AventuraTineret}.docx
Radu Tudoran - La nord de noi insine 0.8 '{AventuraTineret}.docx
Radu Tudoran - Maria si marea 0.9 '{AventuraTineret}.docx
Radu Tudoran - Sfarsit de Mileniu - V1 Casa domnului Alcibiade 3.0 '{AventuraTineret}.docx
Radu Tudoran - Sfarsit de Mileniu - V2 Retragerea fara torte 3.0 '{AventuraTineret}.docx
Radu Tudoran - Sfarsit de Mileniu - V3 Iesirea la mare 3.0 '{AventuraTineret}.docx
Radu Tudoran - Sfarsit de Mileniu - V4 Victoria neinaripata 2.0 '{AventuraTineret}.docx
Radu Tudoran - Sfarsit de Mileniu - V5 Privighetoarea de ziua 3.0. '{AventuraTineret}.docx
Radu Tudoran - Sfarsit de Mileniu - V6 Suta una lovituri de tun 3.0 '{AventuraTineret}.docx
Radu Tudoran - Sfarsit de Mileniu - V7 Sub 0 grade 2.0 '{AventuraTineret}.docx
Radu Tudoran - Toate panzele sus! 2.0 '{AventuraTineret}.docx
Radu Tudoran - Un port la rasarit 3.0 '{AventuraTineret}.docx

./Radu Valentin:
Radu Valentin - Jean Bart V1 1.0 '{ClubulTemerarilor}.docx
Radu Valentin - Jean Bart V2 1.0 '{ClubulTemerarilor}.docx

./Radu Vasile:
Radu Vasile - Cursa pe contrasens 0.99 '{Politica}.docx

./Rafael Abalos:
Rafael Abalos - Grimpow 1.0 '{SF}.docx
Rafael Abalos - Kot 1.0 '{SF}.docx

./Rafael Courtoisie:
Rafael Courtoisie - Leac sfant 1.0 '{Politista}.docx

./Rafael Ferlosio:
Rafael Ferlosio - Peripetiile lui Alfanhui 0.99 '{Literatura}.docx
Rafael Ferlosio - Raul Jarama 0.99 '{Literatura}.docx

./Rafaello Giovagnoli:
Rafaello Giovagnoli - Spartacus 1.0 '{AventuraIstorica}.docx

./Rafael Sabatini:
Rafael Sabatini - Odiseea capitanului Blood 2.0 '{Aventura}.docx
Rafael Sabatini - Scaramouche 1.0 '{Aventura}.docx
Rafael Sabatini - Vulturul marilor 1.0 '{Aventura}.docx

./Ragnar Jonasson:
Ragnar Jonasson - Orb in zapada 1.0 '{Politista}.docx

./Rainer Maria Rilke:
Rainer Maria Rilke - Poeme alese 0.99 '{Versuri}.docx
Rainer Maria Rilke - Versuri 1.0 '{Versuri}.docx

./Ralph Edward Woodrow:
Ralph Edward Woodrow - Religia tainica a Babilonului 0.8 '{Istorie}.docx

./Ralph Hayes:
Ralph Hayes - Comando in inima junglei 1.0 '{ActiuneComando}.docx
Ralph Hayes - Santinela iadului 1.0 '{ActiuneComando}.docx

./Ralph Peters:
Ralph Peters - Contraatac 1.0 '{SF}.docx
Ralph Peters - Flacari din cer 1.0 '{Suspans}.docx

./Raluca Butnariu:
Raluca Butnariu - Crepuscul 0.99 '{Dragoste}.docx
Raluca Butnariu - V1 Privilegii 0.99 '{Dragoste}.docx
Raluca Butnariu - V2 Umbra noptii 0.99 '{Dragoste}.docx
Raluca Butnariu - V3 Deziluzii 0.99 '{Dragoste}.docx
Raluca Butnariu - V4 Regasire 0.99 '{Dragoste}.docx
Raluca Butnariu - V5 Fara regrete 0.99 '{Dragoste}.docx
Raluca Butnariu - V6 Vapaia-Soarelui 0.99 '{Dragoste}.docx

./Raluca Sas Marinescu:
Raluca Sas Marinescu - A.K.A. 0.99 '{Teatru}.docx

./Raluca Tomescu:
Raluca Tomescu - Introducere in metafizica divina 1.0 '{Spiritualitate}.docx

./Ramona Kissy:
Ramona Kissy - Pretul iubirii 0.9 '{Romance}.docx
Ramona Kissy - Soarele mai rasare o data 0.99 '{Dragoste}.docx

./Ramona Stewart:
Ramona Stewart - Pasarea de argint 0.9 '{Dragoste}.docx

./Ramon Sender:
Ramon Sender - Aventura pe Amazoane 0.9 '{AventuraIstorica}.docx

./Ramsey Campbell:
Ramsey Campbell - Copyright depasit 1.0 '{SF}.docx

./Ramtha:
Ramtha - Cartea alba 0.8 '{Spiritualitate}.docx
Ramtha - Cine suntem noi de fapt 0.7 '{Spiritualitate}.docx
Ramtha - Creaza-ti ziua 0.2 '{Spiritualitate}.docx
Ramtha - Dumnezei uitati, trezindu-se 0.6 '{Spiritualitate}.docx
Ramtha - Extras din ghidul incepatorului pentru crearea realitatii 0.5 '{Spiritualitate}.docx
Ramtha - Extras din reflectiile unui maestru despre istoria umanitatii V1 0.9 '{Spiritualitate}.docx
Ramtha - Extras din reflectiile unui maestru despre istoria umanitatii V2 0.7 '{Spiritualitate}.docx
Ramtha - Extras din reflectiile unui maestru despre istoria umanitatii V3 0.7 '{Spiritualitate}.docx
Ramtha - Gandul obisnuit ne creeaza viata de zi cu zi 0.7 '{Spiritualitate}.docx
Ramtha - Harta traseului unui maestru 0.6 '{Spiritualitate}.docx
Ramtha - Magia 0.7 '{Spiritualitate}.docx
Ramtha - Mai sus decat ingerii 0.7 '{Spiritualitate}.docx
Ramtha - Reflectiile unui maestru despre evolutia umanitatii V1 0.6 '{Spiritualitate}.docx
Ramtha - Reflectiile unui maestru despre evolutia umanitatii V2 0.2 '{Spiritualitate}.docx
Ramtha - Schimbarea liniei temporale a destinului nostru 0.7 '{Spiritualitate}.docx
Ramtha - Tehnici 0.5 '{Spiritualitate}.docx
Ramtha - Traversand raul 0.7 '{Spiritualitate}.docx
Ramtha - Ultimul vals al tiranilor - Profetia revizuita 0.4 '{Spiritualitate}.docx

./Randall Frakes & W. H. Wisher:
Randall Frakes & W. H. Wisher - Terminator - V1 Terminator 2.0 '{SF}.docx
Randall Frakes & W. H. Wisher - Terminator - V2 Ziua judecatii 2.2 '{SF}.docx

./Rani Manicka:
Rani Manicka - Zeita orezului 1.0 '{Literatura}.docx

./Ransom Riggs:
Ransom Riggs - Miss Peregrine - V1 Caminul copiilor deosebiti 1.0 '{Literatura}.docx
Ransom Riggs - Miss Peregrine - V2 Orasul pustiu 1.0 '{Literatura}.docx
Ransom Riggs - Miss Peregrine - V3 Biblioteca sufletelor 1.0 '{Literatura}.docx

./Rawi Hage:
Rawi Hage - Gandacul 1.0 '{Literatura}.docx

./Ray Baker:
Ray Baker - Serpoaica din Kyoto 1.0 '{Politista}.docx

./Ray Bradbury:
Ray Bradbury - Aici sunt tigri 1.0 '{SF}.docx
Ray Bradbury - Balaurul 1.1 '{SF}.docx
Ray Bradbury - Cosmaruri in Armageddon 0.99 '{SF}.docx
Ray Bradbury - Cronici martiene 0.99 '{SF}.docx
Ray Bradbury - Fahrenheit 451 2.0 '{SF}.docx
Ray Bradbury - Masinaria zburatoare 1.0 '{SF}.docx
Ray Bradbury - Omul ilustrat 1.0 '{SF}.docx

./Raymond Chandler:
Raymond Chandler - Adio, frumoasa mea 2.0 '{Politista}.docx
Raymond Chandler - Avantaj... Marlowe 2.0 '{Politista}.docx
Raymond Chandler - Crima de mantuiala 1.0 '{Politista}.docx
Raymond Chandler - Doamna din lac 0.8 '{Politista}.docx
Raymond Chandler - Fereastra de sus 1.0 '{Politista}.docx
Raymond Chandler - Misterul crimelor de la Little Fawn Lake 1.0 '{Politista}.docx
Raymond Chandler - Nimic de vanzare 1.0 '{Politista}.docx
Raymond Chandler - Ramas bun pentru vecie 2.0 '{Politista}.docx
Raymond Chandler - Simpla arta a crimei 0.99 '{Politista}.docx
Raymond Chandler - Somnul de veci 2.0 '{Politista}.docx
Raymond Chandler - Ucigas in ploaie 1.0 '{Politista}.docx

./Raymond Drake:
Raymond Drake - Astralii din Atlantida 0.9 '{Spiritualitate}.docx

./Raymond E. Feist:
Raymond E. Feist - Magician 1.0 '{SF}.docx

./Raymond F. Jones:
Raymond F. Jones - Creierele cibernetice 2.0 '{SF}.docx
Raymond F. Jones - Extraterestrul 1.0 '{SF}.docx
Raymond F. Jones - Locuitorul celor doua lumi 1.0 '{SF}.docx
Raymond F. Jones - Luna ucigasa 1.0 '{SF}.docx

./Raymond Fowler:
Raymond Fowler - Cazul Andreasson V1 0.8 '{MistersiStiinta}.docx

./Raymond Khoury:
Raymond Khoury - Templieri - V1 Ultimul templier 1.0 '{AventuraIstorica}.docx
Raymond Khoury - Templieri - V2 Sancturul 1.0 '{AventuraIstorica}.docx
Raymond Khoury - Templieri - V3 Salvarea templierilor 1.0 '{AventuraIstorica}.docx

./Raymond L. Wejl:
Raymond L. Wejl - Primul contact 1.0 '{SF}.docx

./Raymond Queneau:
Raymond Queneau - Suntem mereu prea buni cu femeile 0.99 '{Literatura}.docx
Raymond Queneau - Zazie in metrou 0.99 '{Literatura}.docx

./Ray Nelson:
Ray Nelson - Ora 8 dimineata 1.0 '{SF}.docx

./Ray Willbruce:
Ray Willbruce - Hammet intergalactic 1.0 '{SF}.docx

./Razvan Petrescu:
Razvan Petrescu - Mici schimbari de atitudine 0.99 '{ProzaScurta}.docx
Razvan Petrescu - Sansa 0.99 '{ProzaScurta}.docx

./Razvan Radulescu:
Razvan Radulescu - Teodosie cel mic 0.8 '{Literatura}.docx

./Razvan Tupa:
Razvan Tupa - Fetis 0.9 '{Poezie}.docx

./Rebecca Hanover:
Rebecca Hanover - The Similars - V1 Clonele 1.0 '{Literatura}.docx

./Rebecca Stratton:
Rebecca Stratton - Mireasa lui Romano 0.99 '{Dragoste}.docx

./Reggie Oliver:
Reggie Oliver - Printre morminte 0.99 '{Horror}.docx

./Regina Maria:
Regina Maria - Povestea vietii mele V1 1.0 '{Biografie}.docx
Regina Maria - Tara mea 1.0 '{Politica}.docx

./Regine Andry:
Regine Andry - O fata singura. Zapezile verii 1.0 '{Dragoste}.docx
Regine Andry - O femeie singura 1.0 '{Dragoste}.docx

./Regine Dumay:
Regine Dumay - Arta de a face dragoste cu un barbat 0.7 '{DezvoltarePersonala}.docx

./Reito Jeno:
Reito Jeno - Carantina la Grand Hotel. Ciclonul blond 1.0 '{Politista}.docx

./Remus Luca:
Remus Luca - Drum de soarta si razbunare 1.0 '{IstoricaRo}.docx

./Remus Radina:
Remus Radina - Testamentul din morga 0.9 '{Razboi}.docx

./Renato Pestriniero:
Renato Pestriniero - Cuibul de dincolo de umbra 0.9 '{SF}.docx

./Rene Barjavel:
Rene Barjavel - Cataclismul 0.9 '{Literatura}.docx
Rene Barjavel - Tarendol 1.0 '{Dragoste}.docx

./Rene Chenaux Repond:
Rene Chenaux Repond - De ce cred in Isus 0.9 '{Spiritualitate}.docx

./Rene Descartes:
Rene Descartes - Discurs asupra metodei 1.0 '{Filozofie}.docx

./Renee Ahdieh:
Renee Ahdieh - The wrath & the dawn - V1 Urgia si zorile 1.0 '{AventuraIstorica}.docx

./Renee Dunan:
Renee Dunan - La un pas de moarte 1.0 '{Detectiv}.docx

./Renee Knight:
Renee Knight - Avertisment 1.0 '{Literatura}.docx

./Renee Shann:
Renee Shann - Fantana legamintelor 0.9 '{Romance}.docx
Renee Shann - Murmurul valurilor 0.99 '{Dragoste}.docx
Renee Shann - Surprizele unei casatorii 0.99 '{Dragoste}.docx
Renee Shann - Tratament pentru singuratate 0.99 '{Romance}.docx

./Rene Thevenin:
Rene Thevenin - Prizoniera monstrului 1.0 '{Tineret}.docx

./Resat Nuri Guntekin:
Resat Nuri Guntekin - Pitulicea 1.0 '{Literatura}.docx
Resat Nuri Guntekin - Povestiri 1.0 '{Literatura}.docx

./Retete Culinare:
Retete Culinare - 1800 retete culinare practice 1.0 '{Alimentatie}.docx
Retete Culinare - Prajituri de la Edith 1.0 '{Alimentatie}.docx

./Revista:
Revista - Argos Nr. 1, aprilie 2013 0.9 '{SF}.docx
Revista - Argos Nr. 2, iunie 2013 0.9 '{SF}.docx
Revista - Argos Nr. 3, august 2013 0.9 '{SF}.docx
Revista - Argos Nr. 4, octombrie 2013 0.9 '{SF}.docx
Revista - Argos Nr. 5, decembrie 2013 0.9 '{SF}.docx
Revista - Argos Nr. 6, februarie 2014 0.9 '{SF}.docx
Revista - Argos Nr. 7, aprilie 2014 0.9 '{SF}.docx
Revista - Argos Nr. 8, iunie 2014 0.9 '{SF}.docx
Revista - Argos Nr. 9, toamna 2014 0.9 '{SF}.docx
Revista - Argos Nr. 10, iarna 2014-2015 0.9 '{SF}.docx
Revista - Argos Nr. 11, primavara 2015 0.9 '{SF}.docx

./Rex Harwey:
Rex Harwey - Canionul blestemat 1.0 '{Western}.docx

./Rex Pickett:
Rex Pickett - Beat crita 1.0 '{Literatura}.docx

./Rex Stout:
Rex Stout - Capcana pentru FBI 1.0 '{Politista}.docx

./Rex Warner:
Rex Warner - Iulius Caesar 1.0 '{Biografie}.docx

./Rhidian Brook:
Rhidian Brook - Dragoste si ura 1.0 '{Literatura}.docx

./Rhoda Belleza:
Rhoda Belleza - 1000 de Ceruri - V1 Imparateasa celor o mie de ceruri 1.0 '{Literatura}.docx

./Rhoda Nelson:
Rhoda Nelson - Propunere matrimoniala 0.99 '{Romance}.docx

./Rhonda Byrne:
Rhonda Byrne - Invataturi zilnice 1.0 '{DezvoltarePersonala}.docx
Rhonda Byrne - Magia 1.0 '{DezvoltarePersonala}.docx
Rhonda Byrne - Puterea 1.0 '{DezvoltarePersonala}.docx
Rhonda Byrne - Secretul 3.0 '{DezvoltarePersonala}.docx

./Richard Bach:
Richard Bach - Iluzii 0.8 '{Diverse}.docx
Richard Bach - Pescarusul Johnatan Livingston 0.99 '{Diverse}.docx

./Richard C. Knott:
Richard C. Knott - Cavalerii Pisicilor Negre ataca 1.0 '{Razboi}.docx

./Richard C. Morais:
Richard C. Morais - Madame Mallory si micul bucatar indian 1.0 '{Diverse}.docx

./Richard Condon:
Richard Condon - Familia Prizzi 0.99 '{Thriller}.docx

./Richard Dawkins:
Richard Dawkins - Ceasornicarul orb 1.0 '{MistersiStiinta}.docx
Richard Dawkins - Gena egoista 0.7 '{MistersiStiinta}.docx
Richard Dawkins - O curiozitate fara margini 1.0 '{MistersiStiinta}.docx
Richard Dawkins - Un rau pornit din Eden 1.0 '{MistersiStiinta}.docx

./Richard Doetsch:
Richard Doetsch - Hotii paradisului 0.99 '{AventuraIstorica}.docx

./Richard Essex:
Richard Essex - Sapte semnaturi 1.0 '{Politista}.docx

./Richard Hilary Weber:
Richard Hilary Weber - Ars 0.9 '{Literatura}.docx

./Richard Kadrey:
Richard Kadrey - Modificat pentru a ucide 1.0 '{SF}.docx
Richard Kadrey - Natura moarta cu apocalipsa 1.0 '{SF}.docx

./Richard Leakey:
Richard Leakey - Originea omului 1.0 '{MistersiStiinta}.docx

./Richard Matheson:
Richard Matheson - Scamatorie 0.99 '{Diverse}.docx
Richard Matheson - Sorry, right number 1.0 '{SF}.docx
Richard Matheson - Sunt o legenda 3.0 '{Thriller}.docx

./Richard Mckenna:
Richard Mckenna - Vanatorule, intoarce-te acasa 0.99 '{SF}.docx

./Richard Moran:
Richard Moran - Ziua de apoi. 10 scenarii ale sfarsitului lumii 1.0 '{Diverse}.docx

./Richard Morgan:
Richard Morgan - Omul negru V1 1.0 '{SF}.docx
Richard Morgan - Omul negru V2 1.0 '{SF}.docx
Richard Morgan - Takeshi Kovacs - V1 Carbon modificat 1.0 '{SF}.docx
Richard Morgan - Takeshi Kovacs - V2 Portalul ingerilor 1.0 '{SF}.docx

./Richard Neebel:
Richard Neebel - Jocul razboiului 1.0 '{ActiuneRazboi}.docx

./Richard Paul Evans:
Richard Paul Evans - Floarea soarelui 0.9 '{Dragoste}.docx

./Richard Pipes:
Richard Pipes - Scurta istorie a revolutiei ruse 1.0 '{Istorie}.docx

./Richard Rohmer:
Richard Rohmer - Rommel si Patton 1.0 '{ActiuneComando}.docx

./Richard Rudd:
Richard Rudd - Cheile genelor 0.6 '{MistersiStiinta}.docx

./Richard Russo:
Richard Russo - Empire Falls 0.8 '{Literatura}.docx

./Richard Sky:
Richard Sky - Spectacolul trebuie sa continue 0.99 '{Biografie}.docx

./Richard Stark:
Richard Stark - Vanatorul 1.0 '{ActiuneComando}.docx

./Richard Terrille:
Richard Terrille - Atac in Ecuador 1.0 '{ActiuneComando}.docx

./Richard W. de Haan:
Richard W. de Haan - Martirii din catacombe 0.9 '{AventuraIstorica}.docx

./Richard Wagner:
Richard Wagner - Pelerinaj la Beethoven 0.99 '{Diverse}.docx

./Richard Walton:
Richard Walton - Galaxy 1.0 '{SF}.docx

./Richard Wurmbrand:
Richard Wurmbrand - Cu Dumnezeu in subterana 0.7 '{Comunism}.docx
Richard Wurmbrand - Marx si Satan 1.0 '{Comunism}.docx

./Richard Yates:
Richard Yates - Parada de Paste 1.0 '{Literatura}.docx
Richard Yates - Revolutionary road 1.0 '{Literatura}.docx

./Richelle Mead:
Richelle Mead - Academia vampirilor - V1 Academia vampirilor 0.99 '{Vampiri}.docx
Richelle Mead - Academia vampirilor - V2 Initierea 0.99 '{Vampiri}.docx
Richelle Mead - Academia vampirilor - V3 Atingerea umbrei 0.99 '{Vampiri}.docx
Richelle Mead - Academia vampirilor - V4 Juramant de sange 0.99 '{Vampiri}.docx
Richelle Mead - Academia vampirilor - V5 Limitele spiritului 0.99 '{Vampiri}.docx
Richelle Mead - Academia vampirilor - V6 Sacrificiul final 0.99 '{Vampiri}.docx
Richelle Mead - Georgina kincaid - V1 Tristeti de sucub 1.0 '{Vampiri}.docx
Richelle Mead - Georgina kincaid - V2 Nopti de sucub 0.8 '{Vampiri}.docx
Richelle Mead - Georgina kincaid - V3 Vise de sucub 0.99 '{Vampiri}.docx
Richelle Mead - Georgina kincaid - V4 Patimi de sucub 0.99 '{Vampiri}.docx
Richelle Mead - Georgina kincaid - V5 Taine de sucub 0.99 '{Vampiri}.docx
Richelle Mead - Georgina kincaid - V6 Dezvaluiri de sucub 0.99 '{Vampiri}.docx

./Rick Campbell:
Rick Campbell - Jocul puterii 1.0 '{SF}.docx

./Rick Joyner:
Rick Joyner - Cercetarea finala 0.99 '{Spiritualitate}.docx

./Rick Moody:
Rick Moody - Furtuna de gheata 1.0 '{Literatura}.docx

./Rick Riordan:
Rick Riordan - Cronicile Familiei Kane - V1 Piramida rosie 1.0 '{AventuraIstorica}.docx
Rick Riordan - Cronicile Familiei Kane - V2 Tronul de foc 1.0 '{AventuraIstorica}.docx
Rick Riordan - Cronicile Familiei Kane - V3 Umbra sarpelui 1.0 '{AventuraIstorica}.docx
Rick Riordan - Magnus Chase si Zeii din Asgard - V1 Sabia verii 1.0 '{AventuraIstorica}.docx
Rick Riordan - Percy Jackson si Olimpienii - V1 Hotul fulgerului 1.0 '{AventuraIstorica}.docx
Rick Riordan - Percy Jackson si Olimpienii - V2 Marea monstrilor 1.0 '{AventuraIstorica}.docx
Rick Riordan - Percy Jackson si Olimpienii - V3 Blestemul titanului 1.0 '{AventuraIstorica}.docx

./Rick Yancey:
Rick Yancey - Al Cincilea Val - V1 Al cincilea val 1.0 '{SF}.docx
Rick Yancey - Al Cincilea Val - V2 Marea nesfarsita 1.0 '{SF}.docx
Rick Yancey - Al Cincilea Val - V3 Ultima stea 1.0 '{SF}.docx

./Ridley Pearson:
Ridley Pearson - Prabusirea 1.1 '{Suspans}.docx

./Riley Morse:
Riley Morse - Nori de furtuna 0.99 '{Dragoste}.docx
Riley Morse - Virtutea Cristei 0.99 '{Dragoste}.docx

./Rina Frank:
Rina Frank - Toate casele au nevoie de un balcon 1.0 '{Dragoste}.docx

./Rino Cammilleri:
Rino Cammilleri - Justitie divina 1.0 '{Politista}.docx

./Rita Herron:
Rita Herron - Banuiala 0.99 '{Dragoste}.docx
Rita Herron - Logodna falsa 0.99 '{Dragoste}.docx
Rita Herron - Semnul 0.9 '{Dragoste}.docx
Rita Herron - Templul lunii 0.99 '{Dragoste}.docx

./Rita Nightingale & David Porter:
Rita Nightingale & David Porter - Eliberata pe viata 0.8 '{Diverse}.docx

./Roald Dahl:
Roald Dahl - Charlie si fabrica de ciocolata 0.99 '{Tineret}.docx
Roald Dahl - Matilda 1.0 '{Tineret}.docx

./Robbie Hurt:
Robbie Hurt - Hazard 0.99 '{Dragoste}.docx
Robbie Hurt - Pentru dragostea mea 0.9 '{Romance}.docx

./Robert A. Heinlein:
Robert A. Heinlein - Infanteria stelara 3.1 '{SF}.docx
Robert A. Heinlein - Luna e o doamna cruda 2.1 '{SF}.docx
Robert A. Heinlein - Manuitorii de zombi 0.9 '{SF}.docx
Robert A. Heinlein - Papusarii 1.0 '{SF}.docx
Robert A. Heinlein - Pilotul Jones 1.9 '{SF}.docx
Robert A. Heinlein - Si el construi o casa in dunga 2.0 '{SF}.docx
Robert A. Heinlein - Stea dubla 0.6 '{SF}.docx
Robert A. Heinlein - Strain in tara straina 2.0 '{SF}.docx
Robert A. Heinlein - Univers 2.0 '{SF}.docx
Robert A. Heinlein - Voi, zombilor 2.0 '{SF}.docx

./Roberta Michels:
Roberta Michels - Feriti-va de necunoscuti 0.99 '{Dragoste}.docx
Roberta Michels - Logodnic sub acoperire 0.8 '{Dragoste}.docx
Roberta Michels - Pavilionul de vara 0.99 '{Romance}.docx
Roberta Michels - Taramul fermecat 0.9 '{Dragoste}.docx

./Robert Belfiore:
Robert Belfiore - Oul lui Zeltar 0.9 '{SF}.docx

./Robert Bryndza:
Robert Bryndza - Erika Foster - V1 Fata din gheata 1.0 '{Politista}.docx

./Robert Charles Wilson:
Robert Charles Wilson - Julian. O poveste de Craciun 2.0 '{SF}.docx
Robert Charles Wilson - V1 Turbion 3.0 '{SF}.docx
Robert Charles Wilson - V2 Axa 2.0 '{SF}.docx

./Robert Charoux:
Robert Charoux - Cartea secretelor tradate 1.0 '{MistersiStiinta}.docx

./Robert Charroux:
Robert Charroux - Cartea cartilor 1.0 '{MistersiStiinta}.docx

./Robert Cialdini:
Robert Cialdini - Psihologia persuasiunii 0.99 '{Psihologie}.docx

./Robert David:
Robert David - Razboiul lumilor paralele 0.99 '{Diverse}.docx

./Robert Delaite:
Robert Delaite - Nopti insangerate sub palmieri 0.8 '{ActiuneComando}.docx

./Robert Dugoni:
Robert Dugoni - David Sloane - V1 Stapanul juriului 1.0 '{Diverse}.docx
Robert Dugoni - Victimele puterii 1.0 '{Diverse}.docx

./Robert E. Howard:
Robert E. Howard - Conan 1.0 '{AventuraIstorica}.docx

./Robert Fawcett:
Robert Fawcett - Arme pentru Bosnia 0.5 '{ActiuneComando}.docx
Robert Fawcett - Irak teroarea kurda 0.5 '{ActiuneComando}.docx

./Robert Ferrigno:
Robert Ferrigno - Ruga pentru asasin 1.0 '{Suspans}.docx

./Robert Finn:
Robert Finn - Initiatul 0.9 '{Thriller}.docx

./Robert Franklin Young:
Robert Franklin Young - Septembrie avea 30 de zile 1.0 '{Diverse}.docx

./Robert Gaillard:
Robert Gaillard - I-a inghitit padurea 1.0 '{Aventura}.docx

./Robert Galbraith:
Robert Galbraith - Alb letal 1.0 '{Politista}.docx
Robert Galbraith - Cormoran Strike - V1 Chemarea cucului 1.0 '{Politista}.docx
Robert Galbraith - Cormoran Strike - V2 Viermele de matase 1.0 '{Politista}.docx
Robert Galbraith - Cormoran Strike - V3 Cariera malefica 1.0 '{Politista}.docx

./Robert Graves:
Robert Graves - Eu, Claudius Imparat 1.0 '{AventuraIstorica}.docx
Robert Graves - Miturile Greciei Antice 1.0 '{Istorie}.docx

./Robert Greene & Joost Elffers:
Robert Greene & Joost Elffers - Succes si putere. 48 de legi 1.0 '{DezvoltarePersonala}.docx

./Robert Harris:
Robert Harris - Arhanghelsk 1.0 '{Literatura}.docx
Robert Harris - Marioneta 0.9 '{Literatura}.docx
Robert Harris - Pompei 1.0 '{AventuraIstorica}.docx

./Robert Heinlein:
Robert Heinlein - Stea dubla 0.8 '{SF}.docx

./Robert Holdstock:
Robert Holdstock - Mitago - V1 Mitago 1.0 '{SF}.docx
Robert Holdstock - Mitago - V2 Lavondyss 1.0 '{SF}.docx

./Robert J. Sawyer:
Robert J. Sawyer - Alegerea lui Hobson 1.0 '{SF}.docx
Robert J. Sawyer - Flashforward 1.0 '{SF}.docx
Robert J. Sawyer - Programatorul divin 1.0 '{SF}.docx

./Robert Jackson Bennet:
Robert Jackson Bennet - Orasele Divine - V1 Orasul scarilor 1.0 '{AventuraIstorica}.docx

./Robert James Waller:
Robert James Waller - Podurile din Madison County 2.0 '{Literatura}.docx

./Robert Jean Boulan:
Robert Jean Boulan - In imparatia mortii albe 1.0 '{Tineret}.docx

./Robert Jordan:
Robert Jordan - Roata Timpului - V1 Ochiul lumii 2.0 '{SF}.docx
Robert Jordan - Roata Timpului - V2 In cautarea cornului 2.0 '{SF}.docx
Robert Jordan - Roata Timpului - V3 Dragonul renascut 2.0 '{SF}.docx
Robert Jordan - Roata Timpului - V4 Umbra se intinde 1.0 '{SF}.docx
Robert Jordan - Roata Timpului - V5 Focurile cerului 1.0 '{SF}.docx
Robert Jordan - Roata Timpului - V6 Seniorul haosului 1.0 '{SF}.docx
Robert Jordan - Roata Timpului - V7 Coroana de sabii 1.0 '{SF}.docx
Robert Jordan - Roata Timpului - V8 Calea pumnalelor 2.0 '{SF}.docx

./Robert K. Ressler & Tom Shachtman:
Robert K. Ressler & Tom Shachtman - Vanator de ucigasi 1.0 '{ActiuneComando}.docx

./Robert King:
Robert King - Sarpele albastru 1.0 '{Politista}.docx

./Robert Kocher:
Robert Kocher - Intoarce-te! 0.9 '{ProzaScurta}.docx

./Robert Kyle:
Robert Kyle - Inmormantare la cabaret 1.0 '{Politista}.docx

./Robert L. Stevenson:
Robert L. Stevenson - Rapit de pirati 1.0 '{AventuraTineret}.docx

./Robert L. Wolke:
Robert L. Wolke - Ce i-a spus Einstein bucatarului sau 1.0 '{Bucatarie}.docx

./Robert Lanza & Bob Berman:
Robert Lanza & Bob Berman - Biocentrismul 1.0 '{MistersiStiinta}.docx

./Robert Lazu:
Robert Lazu - Divina com. medi@ 0.2 '{ProzaScurta}.docx

./Robert Lohr:
Robert Lohr - Automatul de sah 0.99 '{Literatura}.docx

./Robert Lomas:
Robert Lomas - Dezvaluirea secretului lui Hiram 1.0 '{MistersiStiinta}.docx

./Robert Louis Stevenson:
Robert Louis Stevenson - Diavolul din clondir 2.0 '{Aventura}.docx
Robert Louis Stevenson - Insula comorii 4.2 '{AventuraTineret}.docx
Robert Louis Stevenson - Markheim 0.99 '{Aventura}.docx
Robert Louis Stevenson - Rapit de pirati 0.99 '{Aventura}.docx
Robert Louis Stevenson - Razbunarea Seniorului 0.99 '{Aventura}.docx
Robert Louis Stevenson - Sageata Neagra 0.99 '{Aventura}.docx
Robert Louis Stevenson - Saint-Ives sau aventurile unui prizonier francez 0.99 '{Aventura}.docx
Robert Louis Stevenson - Secretul epavei 1.0 '{Aventura}.docx
Robert Louis Stevenson - Seniorul din Ballantrae 0.8 '{Aventura}.docx
Robert Louis Stevenson - Straniul caz al doctorului Jekyll si al domnului Hyde 2.0 '{Aventura}.docx

./Robert Louis Stevenson & Lloyd Osbourne:
Robert Louis Stevenson & Lloyd Osbourne - Un colet cu bucluc 0.99 '{Aventura}.docx

./Robert Ludlum:
Robert Ludlum - Agenda lui Icarus 1.0 '{Suspans}.docx
Robert Ludlum - Al patrulea Reich 2.0 '{Suspans}.docx
Robert Ludlum - Bourne - V1 Identitatea lui Bourne 4.0 '{Suspans}.docx
Robert Ludlum - Bourne - V2 Suprematia lui Bourne 2.0 '{Suspans}.docx
Robert Ludlum - Bourne - V3 Ultimatumul lui Bourne 4.0 '{Suspans}.docx
Robert Ludlum - Bourne - V4 Mostenirea lui Bourne 2.0 '{Suspans}.docx
Robert Ludlum - Cercul Matarese 3.0 '{Suspans}.docx
Robert Ludlum - Complotul generalilor 2.0 '{Suspans}.docx
Robert Ludlum - Compromisul 3.0 '{Suspans}.docx
Robert Ludlum - Deceptia lui Prometeu 1.0 '{Suspans}.docx
Robert Ludlum - Directiva Janson 2.0 '{Suspans}.docx
Robert Ludlum - Documentul Matlock 1.0 '{Suspans}.docx
Robert Ludlum - Drumul spre Gandolfo 1.0 '{Suspans}.docx
Robert Ludlum - Drumul spre Omaha 1.0 '{Suspans}.docx
Robert Ludlum - Experimentul Ambler 1.0 '{Suspans}.docx
Robert Ludlum - Fum verde 1.0 '{Suspans}.docx
Robert Ludlum - Gemenii rivali 1.0 '{Suspans}.docx
Robert Ludlum - Iluzia scorpionilor V1 2.0 '{Suspans}.docx
Robert Ludlum - Iluzia scorpionilor V2 2.0 '{Suspans}.docx
Robert Ludlum - Manuscrisul lui Chancellor 1.2 '{Suspans}.docx
Robert Ludlum - Mostenirea Scarlatti 1.0 '{Suspans}.docx
Robert Ludlum - Mozaicul lui Parsifal 2.0 '{Suspans}.docx
Robert Ludlum - Numaratoarea inversa Matarese 2.0 '{Suspans}.docx
Robert Ludlum - Operatiunea Omega 1.1 '{Suspans}.docx
Robert Ludlum - Protocolul Sigma 1.0 '{Suspans}.docx
Robert Ludlum - Strategia Bancroft 1.0 '{Suspans}.docx
Robert Ludlum - Testamentul lui Holcroft 2.0 '{Suspans}.docx
Robert Ludlum - Tinutul magic 1.0 '{Suspans}.docx
Robert Ludlum - Tradarea lui Tristan 1.0 '{Suspans}.docx
Robert Ludlum - Weekend Osterman 0.5 '{Suspans}.docx

./Robert Ludlum & Gayle Lynds:
Robert Ludlum & Gayle Lynds - Actiunea Mascarada 1.0 '{Suspans}.docx
Robert Ludlum & Gayle Lynds - Codul Altman 1.0 '{Suspans}.docx
Robert Ludlum & Gayle Lynds - Conspiratia spirala 1.0 '{Suspans}.docx
Robert Ludlum & Gayle Lynds - Marea schimbare 1.0 '{SF}.docx
Robert Ludlum & Gayle Lynds - Mozaic 1.0 '{Suspans}.docx
Robert Ludlum & Gayle Lynds - Optiunea Paris 1.0 '{Suspans}.docx
Robert Ludlum & Gayle Lynds - Proiectul Hades 2.0 '{Suspans}.docx

./Robert Ludlum & James H. Cobb:
Robert Ludlum & James H. Cobb - Evenimentul arctic 1.0 '{Suspans}.docx

./Robert Ludlum & Patrick Larkin:
Robert Ludlum & Patrick Larkin - Operatiunea Lazarus 1.0 '{Suspans}.docx
Robert Ludlum & Patrick Larkin - Vectorul Moscova 2.0 '{Suspans}.docx

./Robert Ludlum & Philip Shelby:
Robert Ludlum & Philip Shelby - Cassandra Compact 1.0 '{Suspans}.docx

./Robert Manta:
Robert Manta - Poezii 0.99 '{Versuri}.docx

./Robert Marius Dinca:
Robert Marius Dinca - Cer fara stele 0.9 '{Diverse}.docx

./Robert Merle:
Robert Merle - Malevil 1.0 '{Literatura}.docx

./Robert Musil:
Robert Musil - Omul fara insusiri V1 0.99 '{Literatura}.docx
Robert Musil - Omul fara insusiri V2 0.99 '{Literatura}.docx

./Roberto Bolano:
Roberto Bolano - Al treilea Reich 1.0 '{Literatura}.docx
Roberto Bolano - Amuleta 1.0 '{Literatura}.docx
Roberto Bolano - Convorbiri telefonice 3.0 '{Literatura}.docx
Roberto Bolano - Detectivii salbatici 0.99 '{Literatura}.docx
Roberto Bolano - Literatura Nazista in America 1.0 '{Biografie}.docx
Roberto Bolano - Lumpen 1.0 '{Literatura}.docx
Roberto Bolano - Nocturna in Chile 0.99 '{Istorie}.docx
Roberto Bolano - O povestioara lumpen 1.0 '{Istorie}.docx
Roberto Bolano - O stea indepartata 0.99 '{Istorie}.docx
Roberto Bolano - Tarfe asasine 0.99 '{Literatura}.docx

./Roberto Quaglia:
Roberto Quaglia - Gandirea stocastica 0.9 '{Eseu}.docx

./Roberto R. Grant:
Roberto R. Grant - Genocid 0.7 '{Necenzurat}.docx
Roberto R. Grant - Trei texte 0.2 '{SF}.docx

./Robert Park:
Robert Park - Stiinta voodoo. Drumul de la prostie la frauda 1.0 '{MistersiStiinta}.docx

./Robert Reed:
Robert Reed - Dragonii din valceaua adanca 1.0 '{SF}.docx

./Robert Rosenberg:
Robert Rosenberg - Camera de montaj 1.0 '{Suspans}.docx
Robert Rosenberg - Teroare la Ierusalim 0.7 '{Suspans}.docx

./Robert Sheckley:
Robert Sheckley - Atestatul de infractor 0.99 '{SF}.docx
Robert Sheckley - Femeia perfecta 1.0 '{SF}.docx
Robert Sheckley - Immortality Inc. 1.0 '{SF}.docx
Robert Sheckley - Monstrii 1.0 '{SF}.docx
Robert Sheckley - Nugent Miller si fetele 0.99 '{SF}.docx
Robert Sheckley - Omega 3.0 '{SF}.docx
Robert Sheckley - Pasarea de paza 1.0 '{SF}.docx
Robert Sheckley - Planeta ieftina 0.99 '{SF}.docx
Robert Sheckley - Potential 0.99 '{SF}.docx
Robert Sheckley - Poveste cu spioni 0.9 '{SF}.docx
Robert Sheckley - Transfer mental 5.0 '{SF}.docx

./Robert Silverberg:
Robert Silverberg - Afacerea relicvelor 1.0 '{SF}.docx
Robert Silverberg - Cer arzator 1.0 '{SF}.docx
Robert Silverberg - Cursa prin microcipuri 0.8 '{SF}.docx
Robert Silverberg - Imparatul si Maula 0.8 '{SF}.docx
Robert Silverberg - Joc crud 0.5 '{SF}.docx
Robert Silverberg - Multipli 0.99 '{SF}.docx
Robert Silverberg - Pasagerii 1.0 '{SF}.docx
Robert Silverberg - Timp al schimbarilor 4.0 '{SF}.docx
Robert Silverberg - Vantul si ploaia 0.99 '{SF}.docx
Robert Silverberg - Vesti bune de la Vatican 1.0 '{SF}.docx

./Robert Silverberg & Bill Fawcett:
Robert Silverberg & Bill Fawcett - Poarta Timpului - V1 Poarta timpului 0.7 '{CalatorieinTimp}.docx
Robert Silverberg & Bill Fawcett - Poarta Timpului - V2 Interfete periculoase 0.9 '{CalatorieinTimp}.docx

./Robert Smith:
Robert Smith - Razboi cu bunicul 1.0 '{Diverse}.docx

./Robert Smythe Hichens:
Robert Smythe Hichens - Dragostea cu sila a profesorului Guildea 0.9 '{SF}.docx

./Robert T. Kiyosaki:
Robert T. Kiyosaki - Scoala de afaceri 1.0 '{Economie}.docx
Robert T. Kiyosaki - Tata bogat, tata sarac 0.99 '{Economie}.docx

./Robert Tine:
Robert Tine - Pururea tanar 1.0 '{Dragoste}.docx

./Robin Cook:
Robin Cook - Clinica Julian 1.0 '{Thriller}.docx
Robin Cook - Creierul 2.0 '{Thriller}.docx
Robin Cook - Frica de moarte 2.0 '{Thriller}.docx
Robin Cook - Intentii periculoase 2.0 '{Thriller}.docx
Robin Cook - Marker 1.0 '{Thriller}.docx
Robin Cook - Rapirea 1.0 '{Thriller}.docx
Robin Cook - Risc asumat 1.0 '{Thriller}.docx
Robin Cook - Socul 1.0 '{Thriller}.docx
Robin Cook - Tratament fatal 1.0 '{Thriller}.docx

./Robin Hobb:
Robin Hobb - Omul Aramiu - V1 Misiunea bufonului 2.0 '{SF}.docx
Robin Hobb - Omul Aramiu - V2 Bufonul de aur 1.0 '{SF}.docx
Robin Hobb - Omul Aramiu - V3 Destinul bufonului V1 1.0 '{SF}.docx
Robin Hobb - Omul Aramiu - V3 Destinul bufonului V2 1.0 '{SF}.docx
Robin Hobb - Trilogia Farsser - 01 Ucenicul asasinului 2.0 '{SF}.docx
Robin Hobb - Trilogia Farsser - 02 Asasinul regal 2.0 '{SF}.docx
Robin Hobb - Trilogia Farsser - 03 Razbunarea asasinului V1 2.0 '{SF}.docx
Robin Hobb - Trilogia Farsser - 03 Razbunarea asasinului V2 2.0 '{SF}.docx

./Robin James:
Robin James - Sampanie si trandafiri 0.7 '{Dragoste}.docx
Robin James - Sarutul vedetei 0.7 '{Romance}.docx

./Robin Neillands:
Robin Neillands - Indraznetii inving 1.0 '{ActiuneComando}.docx

./Robin Sharma:
Robin Sharma - Calugarul care si-a vandut Ferrari-ul 1.0 '{DezvoltarePersonala}.docx
Robin Sharma - Sfantul, surferul si Ceo-ul 1.0 '{DezvoltarePersonala}.docx

./Robyn Young:
Robyn Young - Fratia Templierilor - V1 Anima templi V1 2.0 '{AventuraIstorica}.docx
Robyn Young - Fratia Templierilor - V1 Anima templi V2 2.0 '{AventuraIstorica}.docx
Robyn Young - Fratia Templierilor - V2 Cruciada V1 2.0 '{AventuraIstorica}.docx
Robyn Young - Fratia Templierilor - V2 Cruciada V2 2.0 '{AventuraIstorica}.docx
Robyn Young - Fratia Templierilor - V3 Recviem V1 2.0 '{AventuraIstorica}.docx
Robyn Young - Fratia Templierilor - V3 Recviem V2 2.0 '{AventuraIstorica}.docx
Robyn Young - Rebeliunea - V1 Rebeliunea 1.0 '{AventuraIstorica}.docx
Robyn Young - Rebeliunea - V2 Renegatul 1.0 '{AventuraIstorica}.docx
Robyn Young - Rebeliunea - V3 Regatul 1.0 '{AventuraIstorica}.docx

./Roderick Gordon & Brian Williams:
Roderick Gordon & Brian Williams - Tunele - V1 Tunele 1.0 '{Tineret}.docx
Roderick Gordon & Brian Williams - Tunele - V2 In adancuri 1.1 '{Tineret}.docx
Roderick Gordon & Brian Williams - Tunele - V3 In cadere libera 1.0 '{Tineret}.docx
Roderick Gordon & Brian Williams - Tunele - V4 Mai aproape 1.0 '{Tineret}.docx
Roderick Gordon & Brian Williams - Tunele - V5 Spirala 0.9 '{Tineret}.docx

./Rod Garaway:
Rod Garaway - Soarele insangerat 0.7 '{ActiuneComando}.docx

./Rod Hammer:
Rod Hammer - Duel pe viata si pe moarte 1.0 '{Politista}.docx

./Rodica Bretin:
Rodica Bretin - Efect holografic 0.99 '{Povestiri}.docx

./Rodica Ojog Brasoveanu:
Rodica Ojog Brasoveanu - Andronic - V1 Logofatul de taina 4.0 '{AventuraIstorica}.docx
Rodica Ojog Brasoveanu - Andronic - V2 Agentul secret al lui Altan Bey 3.0 '{AventuraIstorica}.docx
Rodica Ojog Brasoveanu - Andronic - V3 Ochii jupanitei 2.9 '{AventuraIstorica}.docx
Rodica Ojog Brasoveanu - Andronic - V4 Letopisetul de argint 2.0 '{AventuraIstorica}.docx
Rodica Ojog Brasoveanu - Andronic - V5 Vulturul dincolo de cornul lunii 2.0 '{AventuraIstorica}.docx
Rodica Ojog Brasoveanu - Cristescu - V1 Cocosatul are alibi 2.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Cristescu - V2 O bomba pentru revelion 2.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Cristescu - V3 Enigma la mansarda 4.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Istoric - V1 Al cincilea as 4.0 '{AventuraIstorica}.docx
Rodica Ojog Brasoveanu - Istoric - V2 Intalnire la Elisee 2.0 '{AventuraIstorica}.docx
Rodica Ojog Brasoveanu - Istoric - V3 Sa nu ne uitam la ceas 2.0 '{AventuraIstorica}.docx
Rodica Ojog Brasoveanu - Istoric - V4 A inflorit liliacul 2.0 '{AventuraIstorica}.docx
Rodica Ojog Brasoveanu - Melania - V1 Cianura pentru un suras 2.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Melania - V2 Buna seara, Melania 2.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Melania - V3 320 de pisici negre 2.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Melania - V4 Anonima de miercuri 2.1 '{Politista}.docx
Rodica Ojog Brasoveanu - Melania - V5 Disparitia statuii din parc 2.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Melania - V6 O toaleta a la Liz Taylor 2.1 '{Politista}.docx
Rodica Ojog Brasoveanu - Melania - V7 Melania si misterul din parc 1.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Minerva - V1 Spionaj la manastire 2.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Minerva - V2 Omul de la capatul firului 2.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Minerva - V3 Minerva se dezlantuie 2.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Minerva - V4 Plan diabolic 2.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Minerva - V5 Stilet cu sampanie 4.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Minerva - V6 Nopti albe pentru Minerva 2.1 '{Politista}.docx
Rodica Ojog Brasoveanu - Minerva - V7 Violeta din safe 2.1 '{Politista}.docx
Rodica Ojog Brasoveanu - Minerva - V8 Panica la casuta cu zorele 0.99 '{Politista}.docx
Rodica Ojog Brasoveanu - Politista - V0 Fuga 2.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Politista - V0 Sa nu ne uitam la ceas 0.99 '{Politista}.docx
Rodica Ojog Brasoveanu - Politista - V1 Moartea semneaza indescifrabil 2.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Politista - V2 Ancheta in infern 2.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Politista - V3 Apel din necunoscut 2.1 '{Politista}.docx
Rodica Ojog Brasoveanu - Politista - V4 Crima prin mica publicitate 2.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Politista - V5 Cosmar 2.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Politista - V6 Telefonul din bikini 2.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Politista - V7 Poveste imorala 2.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Politista - V8 Un blestem cu domiciliul stabil 4.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Politista - V9 Cutia cu nasturi 2.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Politista - V10 Necunoscuta din congelator 2.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Politista - V11 Razbunarea slutilor 2.0 '{Politista}.docx
Rodica Ojog Brasoveanu - Povestiri - V1 Grasa si proasta 2.0 '{Povestiri}.docx
Rodica Ojog Brasoveanu - Povestiri - V2 Barbatii sunt niste porci 4.0 '{Povestiri}.docx

./Rodica Purnichec:
Rodica Purnichec - Iubire si astrologie 0.99 '{Psihologie}.docx

./Rodolfo Marzano & Giorgio Capuano:
Rodolfo Marzano & Giorgio Capuano - Cativa Maigret italieni 1.0 '{Politista}.docx

./Rodolphe Kasser:
Rodolphe Kasser - Evanghelia dupa Iuda 1.0 '{MistersiStiinta}.docx

./Rodrigues Urbano Tavares:
Rodrigues Urbano Tavares - Valul de caldura 0.99 '{Dragoste}.docx

./Roger Borniche:
Roger Borniche - Povestea unui politist 1.0 '{Politista}.docx

./Roger ZELAZNY:
Roger ZELAZNY - Amber 3.0 '{SF}.docx

./Roger Zelazny:
Roger Zelazny - Amber - V1 Noua printi din Amber 2.0 '{SF}.docx
Roger Zelazny - Amber - V2 Armele din Avalon 2.0 '{SF}.docx
Roger Zelazny - Amber - V3 Semnul unicornului 2.0 '{SF}.docx
Roger Zelazny - Amber - V4 Mana lui Oberon 2.0 '{SF}.docx
Roger Zelazny - Amber - V5 Curtile haosului 2.0 '{SF}.docx
Roger Zelazny - Amber - V6 Atuurile mortii 2.0 '{SF}.docx
Roger Zelazny - Amber - V7 Sangele din Amber 1.0 '{SF}.docx
Roger Zelazny - Amber - V8 Semnul haosului 2.0 '{SF}.docx
Roger Zelazny - Amber - V9 Cavalerul umbrelor 2.0 '{SF}.docx
Roger Zelazny - Amber - V10 Printul haosului 2.0 '{SF}.docx
Roger Zelazny - Drumul iadului 2.0 '{SF}.docx
Roger Zelazny - Inger, inger intunecat 1.0 '{SF}.docx
Roger Zelazny - Lordul luminii 1.0 '{SF}.docx
Roger Zelazny - Nemuritorul 1.0 '{SF}.docx
Roger Zelazny - Un trandafir pentru ecleziast 0.99 '{SF}.docx

./Roland C. Wagner:
Roland C. Wagner - Fragment din cartea marii 0.9 '{Diverse}.docx
Roland C. Wagner - Livida-ti este pielea, rosie privirea 0.9 '{SF}.docx

./Roland Jaccard:
Roland Jaccard - Nebunia 0.99 '{Psihiatrie}.docx

./Romain Gary:
Romain Gary - Ai toata viata inainte 0.9 '{Literatura}.docx

./Romain Puertolas:
Romain Puertolas - Zahera, fetita care a inghitit un nor mare cat Turnul Eiffel 1.0 '{Literatura}.docx

./Romain Rolland:
Romain Rolland - Inima Vrajita - V1 Annette si Silvie 1.0 '{Dragoste}.docx
Romain Rolland - Inima Vrajita - V2 Vara 1.0 '{Dragoste}.docx
Romain Rolland - Inima Vrajita - V3 Mama si fiu 1.0 '{Dragoste}.docx
Romain Rolland - Inima Vrajita - V4 Moartea unei lumi 1.0 '{Dragoste}.docx
Romain Rolland - Inima Vrajita - V5 Zamislirea 1.0 '{Dragoste}.docx

./Romul Munteanu:
Romul Munteanu - Jurnal de carti 1.0 '{Jurnal}.docx

./Romulus Barbulescu:
Romulus Barbulescu - Catharsis 1.5 '{SF}.docx
Romulus Barbulescu - Insulele de aur si argint 1.0 '{SF}.docx
Romulus Barbulescu - Sarpele bland al infinitului 1.0 '{SF}.docx
Romulus Barbulescu - Simbamuenni V1 1.0 '{ClubulTemerarilor}.docx
Romulus Barbulescu - Simbamuenni V2 1.0 '{ClubulTemerarilor}.docx

./Romulus Barbulescu & George Anania:
Romulus Barbulescu & George Anania - Cat de mic poate fi infernul 1.0 '{SF}.docx
Romulus Barbulescu & George Anania - Constelatia din ape 1.0 '{SF}.docx
Romulus Barbulescu & George Anania - Doando 1.0 '{SF}.docx
Romulus Barbulescu & George Anania - Ferma oamenilor de piatra 1.2 '{SF}.docx
Romulus Barbulescu & George Anania - Ochii ei albastri 1.0 '{SF}.docx
Romulus Barbulescu & George Anania - Paralela Enigma 0.9 '{SF}.docx
Romulus Barbulescu & George Anania - Planeta fantomelor albastre 1.0 '{SF}.docx
Romulus Barbulescu & George Anania - Statuia sarpelui 3.0 '{SF}.docx

./Romulus Cojocaru:
Romulus Cojocaru - Aparatorul se apara 1.0 '{Politista}.docx

./Romulus Lal:
Romulus Lal - Operatiunea Metroul 1.0 '{Jurnal}.docx
Romulus Lal - Temerarii 1.0 '{Tineret}.docx

./Romulus Lucian:
Romulus Lucian - Calatorie spre inima 0.2 '{SF}.docx

./Romulus Vianu:
Romulus Vianu - Fauna bufona. Pseudozoologicon 0.99 '{Natura}.docx

./Ronald Cutler:
Ronald Cutler - Pergamentul secret 1.0 '{Thriller}.docx

./Ronan Bennett:
Ronan Bennett - Ultima mutare a regelui 1.0 '{Thriller}.docx

./Roopa Farooki:
Roopa Farooki - Dulciuri amare 1.0 '{Literatura}.docx

./Rosalind Straley:
Rosalind Straley - Pastila 1.0 '{SF}.docx

./Rosamunde Pilcher:
Rosamunde Pilcher - Cautatorii de scoici 1.0 '{Literatura}.docx
Rosamunde Pilcher - Septembrie 1.0 '{Literatura}.docx
Rosamunde Pilcher - Tigrul adormit 1.0 '{Literatura}.docx

./Rosamund Lupton:
Rosamund Lupton - Sora 1.0 '{Thriller}.docx

./Rose Allyn:
Rose Allyn - Amintiri care dor 1.0 '{Romance}.docx
Rose Allyn - Inimi frante 1.0 '{Romance}.docx
Rose Allyn - Misteriosul inger pazitor 1.0 '{Romance}.docx

./Roseanne Hunter:
Roseanne Hunter - Acea vara a renuntarii 1.0 '{Dragoste}.docx
Roseanne Hunter - Acolo unde iti este inima 0.9 '{Romance}.docx

./Rose Estes & Tom Wham:
Rose Estes & Tom Wham - Sabia lui Skryling 1.0 '{SF}.docx

./Rosemary Aubert:
Rosemary Aubert - Cantecul paradisului 1.0 '{Romance}.docx

./Rosemary Simon:
Rosemary Simon - Binecuvantarea zeilor 0.9 '{Dragoste}.docx

./Rosie Nixon:
Rosie Nixon - Amber Green - V1 Stilista 1.0 '{Literatura}.docx

./Rosie Walsh:
Rosie Walsh - Barbatul care n-a mai sunat 1.0 '{Literatura}.docx

./Ross Macdonald:
Ross Macdonald - Coasta Barbara 1.0 '{Politista}.docx
Ross Macdonald - Dricul vargat 1.0 '{Politista}.docx
Ross Macdonald - Moartea lui Jasper 1.0 '{Politista}.docx
Ross Macdonald - Noaptea razbunarii 1.0 '{Politista}.docx

./Roxana Sahanagiu:
Roxana Sahanagiu - Scrisoare catre Jules 1.0 '{SF}.docx

./Roxanne Veletzos:
Roxanne Veletzos - Fetita pe care au lasat-o in urma 1.0 '{Literatura}.docx

./Roy Arundhati:
Roy Arundhati - Dumnezeul lucrurilor marunte 0.9 '{Literatura}.docx

./Roy Hession & Revel Hession:
Roy Hession & Revel Hession - Drumul calvarului 0.9 '{Spiritualitate}.docx

./Rubin Szilard:
Rubin Szilard - Petrecere in barlogul lupilor 1.0 '{Politista}.docx

./Rudolf Steiner:
Rudolf Steiner - Adevar si stiinta ga3 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Antropologia generala ca baza a pedagogiei ga293 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Antroposofie ga234 1.0 '{Spiritualitate}.docx
Rudolf Steiner - A patra dimensiune ga324a V1 1.0 '{Spiritualitate}.docx
Rudolf Steiner - A patra dimensiune ga324a V2 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Apocalipsa lui Ioan ga104 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Arta de a vindeca aprofundata prin meditatie ga316 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Arta educatiei. Discutii de seminar si conferinte asupra planului de invatamant ga295 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Arta educatiei. Metodica si didactica ga294 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Arta in misiunea ei cosmica ga276 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Arta recitarii si declamatiei ga281 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Arta si cunoasterea artei v6 ga271 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Articole asupra organizarii tripartite a organismului social si a problemelor contemporane ga24 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Astronomia si stiintele naturii ga323 0.9 '{Spiritualitate}.docx
Rudolf Steiner - Bazele spiritual-stiintifice pentru prosperarea agriculturii ga327 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Bhagavad-Gita si epistolele lui Pavel ga142 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Cele douasprezece simturi 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Cercetari oculte referitoare la viata dintre moarte si o noua nastere ga140 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Christos si lumea spirituala ga149 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Christos si sufletul uman ga155 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Ciclul anului ca proces de respiratie a pamantului ga223 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Coactionarea dualitatii umane 0.99 '{Spiritualitate}.docx
Rudolf Steiner - Colaborarea dintre medici si pastorii sufletesti ga318 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Conducerea spirituala a omului si a omenirii ga15 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Consideratii esoterice asupra legaturilor karmice - V1 ga235 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Consideratii esoterice asupra legaturilor karmice - V2 ga236 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Consideratii esoterice asupra legaturilor karmice - V3 ga237 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Consideratii esoterice asupra legaturilor karmice - V4 ga238 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Consideratii esoterice asupra legaturilor karmice - V5 ga239 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Consideratii esoterice asupra legaturilor karmice - V6 ga240 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Contraste in evolutia omenirii ga197 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Convorbiri despre natura albinelor ga351 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Crestinismul ca fapt mistic si secretele antichitatii crestine ga8 1.2 '{Spiritualitate}.docx
Rudolf Steiner - Crestinismul esoteric ga130 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Cum actioneaza karma ga34 V3 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Cum il poate regasi omenirea pe Christos ga187 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Cum se dobandesc cunostinte despre lumile superioare ga10 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Cunoasterea lui Hristos ga100 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Cunoastere antroposofica a omului si medicina ga319 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Cunoasterea omului si structurarea invatamantului ga302 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Cunoasterea sufletului si a spiritului ga56 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Cursul anului trait in patru imaginatiuni cosmice ga229 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Cursul de pedagogie curativa ga317 1.0 '{Spiritualitate}.docx
Rudolf Steiner - De la Iisus la Christos ga131 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Despre enigmele sufletului ga21 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Devenirea umana, sufletul lumii si spiritul lumii ga205 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Dezvoltarea sanatoasa a fiintei umane ga303 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Din cronica Akasha. Evanghelia a cincea ga148 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Din cronica Akasha. Stramosii nostri atlanteeni 0.8 '{Spiritualitate}.docx
Rudolf Steiner - Din cronica Akasha ga11 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Dualitatea umana 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Educatia copilului ga34 v5 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Enigmele filosofiei ga18 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Entitatea umana 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Entitatile spirituale in corpurile ceresti ga136 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Esoterismul crestin ga94 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Evanghelia dupa Ioan - V1 ga100 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Evanghelia dupa Ioan - V2 ga103 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Evanghelia dupa Ioan - V3 ga112 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Evanghelia dupa Luca ga114 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Evanghelia dupa Marcu ga139 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Evanghelia dupa Matei ga123 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Evenimentul aparitiei lui Christos in lumea eterica ga118 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Filosofia libertatii ga4 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Fiziologie oculta ga128 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Fiziologie si terapie in conceptia stiintei spirituale ga314 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Forma umana si articularea fortelor 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Fortele spiritual-sufletesti fundamentale ale artei educative ga305 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Friedrich Nietzsche, un luptator impotriva epocii sale ga5 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Gand uman, gand cosmic ga151 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Goethe, vizionarul, alaturi de Schiller, reflexivul 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Ierarhiile spirituale ga110 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Incercarea sufletului ga14 V2 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Indrumari pentru o educatie esoterica ga42 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Intelepciunea primitiva si noua intelepciune apocaliptica 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Intrebari omenesti, raspunsuri cosmice ga213 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Introduceri la scrierile de stiinte naturale ale lui Goethe ga1 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Linii fundamentale ale unei teorii a cunoasterii in conceptia goetheana despre lume ga2 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Lucifer ga34 V1 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Lumea simturilor si lumea spiritului ga134 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Macrocosmos si microcosmos ga119 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Manifestari exterioare ale entitatilor spirituale prin elemente 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Manifestarile karmei ga120 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Metamorfoza cosmica si umana ga175 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Metamorfozele vietii sufletesti - V1 ga58 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Metamorfozele vietii sufletesti - V2 ga59 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Metodica predarii si conditiile de viata ga308 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Metodica si fiinta modelarii vorbirii ga280 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Misiunea lui Mihail ga194 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Misiunea sufletelor popoarelor ga121 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Misterul biblic al Genezei ga122 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Misterul crestin ga97 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Mistica in zorii vietii spirituale a timpului nostru ga7 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Mituri si misterii egiptene ga106 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Moarte pamanteasca si viata cosmica ga181 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Modelarea vorbirii si arta dramatica ga282 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Noua spiritualitate si trairea lui Hristos ga182 ga200 0.8 '{Spiritualitate}.docx
Rudolf Steiner - Observarea naturii ga324 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Omul hieroglifa a cosmosului ga201 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Omul si lumea ga351 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Omul suprasensibil ga231 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Orientul in lumina occidentului ga113 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Originea si telul omului. Notiunile fundamentale ale Teosofiei ga53 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Pazitorul pragului ga14 V3 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Perspective ale evolutiei umanitatii ga204 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Pietre fundamentale pentru cunoasterea misteriului de pe Golgota ga175 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Poarta initierii ga14 V1 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Pragul lumii spirituale ga17 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Principiul economiei spirituale in legatura cu problema reincarnarii ga109 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Psihologie spirituala ga52 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Reincarnarea si karma V1 ga135 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Reincarnarea si karma V2 ga34 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Responsabilitatea omului pentru evolutia lumii ga203 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Secretul temperamentelor umane 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Selectie 1904-1912 - Christian Rosenkreutz si misiunea sa 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Selectie 1904-1915 - Ideea Craciunului si taina eului. Arborele crucii si legenda de aur 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Selectie 1904-1922 - Psihologie spirituala 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Selectie 1904-1924 - Karma si reincarnare 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Selectie 1908-1924 - Fiinte spirituale din natura 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Selectie 1912-1924 - Despre lucrarea ingerilor si a altor entitati ierarhice 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Semne si simboluri ale Craciunului ga54 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Semne si simboluri ale sarbatorii Craciunului ga96 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Spiritualitatea cosmica si fizicul uman ga202 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Stiinta initiatica si cunoasterea astrilor ga228 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Stiinta oculta ga13 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Stiinta spirituala si problema sociala ga34 V4 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Taina sfintei treimi ga214 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Tainele pragului ga147 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Teosofia - Introducere in cunoasterea suprasensibila ga9 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Terapeutica si stiinta spirituala ga313 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Teze antroposofice ga26 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Treptele cunoasterii superioare ga12 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Treptele initierii 0.7 '{Spiritualitate}.docx
Rudolf Steiner - Trepte premergatoare misteriului de pe Golgota ga152 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Trezirea sufletelor ga14 V4 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Un drum spre cunoasterea de sine ga16 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Un exercitiu pentru activarea viziunii karmice 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Universul Pamantul si Omul. Mitologia egipteana si civilizatia contemporana ga105 1.0 '{Spiritualitate}.docx
Rudolf Steiner - Viata intre moarte si o noua nastere ga141 1.0 '{Spiritualitate}.docx

./Russell Banks:
Russell Banks - Deriva continentelor 0.8 '{Diverse}.docx
Russell Banks - Rezervatia 0.7 '{Diverse}.docx

./Ruta Sepetys:
Ruta Sepetys - Adio, New Orleans 1.0 '{Literatura}.docx
Ruta Sepetys - O mare de lacrimi 1.0 '{Literatura}.docx
Ruta Sepetys - Printre tonuri cenusii 1.0 '{Literatura}.docx

./Ruth Hogan:
Ruth Hogan - Colectionarul de obiecte pierdute 1.0 '{Literatura}.docx

./Ruth K. Westheimer:
Ruth K. Westheimer - Enciclopedia sexului 0.7 '{Sanatate}.docx

./Ruth Owen:
Ruth Owen - Amintesteti ca ma iubesti 0.99 '{Romance}.docx
Ruth Owen - Aparatorul 0.99 '{Dragoste}.docx
Ruth Owen - Ape periculoase 0.99 '{Dragoste}.docx
Ruth Owen - Expertul 0.99 '{Romance}.docx
Ruth Owen - Floarea de lotus 0.9 '{Dragoste}.docx
Ruth Owen - Nu totul se cumpara 0.99 '{Dragoste}.docx

./Ruth Rendell:
Ruth Rendell - Cotloane intunecate 1.0 '{Thriller}.docx

./Ruth Ware:
Ruth Ware - Femeia din cabina 1.0 '{Thriller}.docx
Ruth Ware - Intr-o padure intunecata 1.0 '{Thriller}.docx
Ruth Ware - Jocul minciunii 1.0 '{Thriller}.docx

./Ruxandra Cesereanu:
Ruxandra Cesereanu - Decembrie 89 0.8 '{Istorie}.docx
Ruxandra Cesereanu - Imaginarul violent al romanilor 0.8 '{Critica}.docx

./Ruxandra Niculescu:
Ruxandra Niculescu - Nuvele 0.99 '{Nuvele}.docx

./Ruxandra Rascanu:
Ruxandra Rascanu - Psihologie si comunicare 0.9 '{Psihologie}.docx

./Rysa Walker:
Rysa Walker - Dosarele Cronos - V1 Captivi in timp 1.0 '{CalatorieinTimp}.docx

./Ryu Mukarami:
Ryu Mukarami - Albastru nemarginit, aproape transparent 0.99 '{Diverse}.docx

./Ryu Murakami:
Ryu Murakami - Ecstasy 0.3 '{Literatura}.docx
```

